import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_handy.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

 # Prepare Metadata and create H5-File
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


# Establishing a connection to the acceleration sensor
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Key als uuid des Sensors finden und in einer variable speichern:

#Id des sensors auslesen:

with open(path_setup_json) as setup:
    ids = json.load(setup)

ids1 = ids["setup"]

for i in ids1:
    if ids["setup"][i]["type"] == "sensor":
        key = i

# Dictionary erstellen

acceleration_x = []
acceleration_y = []
acceleration_z = []
timestamp = []

data = {key: {"acceleration_x": acceleration_x, "acceleration_y": acceleration_y, "acceleration_z": acceleration_z, "timestamp": timestamp}}

# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

i2c = board.I2C()  # uses board.SCL and board.SDA
accelerometer = adafruit_adxl34x.ADXL345(i2c)

input("Press any key to start measurement... <Ctrl+C> to stop measurement")
try:
    while True:
        
        now = time.time()
        
        print("%f x | %f y | %f z | %fs" % (accelerometer.acceleration[0], accelerometer.acceleration[1], accelerometer.acceleration[2], now))
        
        #Beschleunigungsanteile in die Listen speichern:
        
        acceleration_x.append(accelerometer.acceleration[0])
        acceleration_y.append(accelerometer.acceleration[1])
        acceleration_z.append(accelerometer.acceleration[2])
        timestamp.append(now)
        
        time.sleep(0.1)
    print("")
except KeyboardInterrupt:
    print("done")

# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#Zeit in Kleinere Sekunden anzahl umwandeln:

time0 = data[key]["timestamp"][0]

time = []

for i in data[key]["timestamp"]:
    time.append(i - data[key]["timestamp"][0])

#Gruppen, Datasets und Attribute erstellen:
    
with h5py.File(path_h5_file, "w") as f:
    grp_raw = f.create_group("RawData")
    grp_key = grp_raw.create_group(key)
    
    dset_acceleration_x = grp_key.create_dataset("acceleration_x", data = data[key]["acceleration_x"]) #,  maxshape = None, dtype = 'f'
    dset_acceleration_y = grp_key.create_dataset("acceleration_y", data = data[key]["acceleration_y"])
    dset_acceleration_z = grp_key.create_dataset("acceleration_z", data = data[key]["acceleration_z"])
    dset_timestamp = grp_key.create_dataset("timestamp", data = time)
    
    dset_acceleration_x.attrs["unit"] = "m/s2"
    dset_acceleration_y.attrs["unit"] = "m/s2"
    dset_acceleration_z.attrs["unit"] = "m/s2"
    dset_timestamp.attrs["unit"] = "s"

# ---------------------------------------------------------------------------------------------#4-end

 # Log JSON metadata
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))


