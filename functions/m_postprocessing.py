"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple
import math


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """

    betrag = math.sqrt((x**2)+(y**2)+(z**2))
    
    return betrag

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    length = len(data)
    t_min = np.min(time) #time[0]
    t_max = np.max(time) #time[-1]

    #gleichverteilte zeitpunkte zwischen t_min und t_max erstellen:
    
    t = np.linspace(t_min, t_max, num = length)
    
    #Interpolation des Betrags ausrechnen:
    
    betrag_ip = np.interp(t, time, data)
    
    return(t, betrag_ip)
             
def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    
    length = len(x)
    amplitude = np.fft.fft(x)
    frequenz = np.fft.fftfreq(len(time)) 
    

    return (amplitude, frequenz)